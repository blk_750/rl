// Program main entry point
fn main() {
	// - Initialization -
	const SCREEN_WIDTH: i32 = 800;
	const SCREEN_HEIGHT: i32 = 450;

	let font = rl::Font::BUILTIN;

	rl::start_graphics!(
		SCREEN_WIDTH,
		SCREEN_HEIGHT,
		"raylib [core] example - basic window",
	);

	rl::set_target_fps(60); // Set our game to run at 60 frames-per-second

	// ------------------

	// Main game loop
	// Detect window close button or ESC key
	while !rl::window_should_close() {
		// - Update -
		// TODO: Update your variables here
		// ----------

		// - Draw -
		rl::draw! {{
			rl::clear_background(rl::Color::RAY_WHITE);

			font.print()
				.pos(190, 200)
				.size(20.)
				.tint(rl::Color::LIGHT_GRAY)
				.commit("Congrats! You created your first window!");
		}}

		// --------
	}
}
