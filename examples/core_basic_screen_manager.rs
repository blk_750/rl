// Types and Structures Definition
enum GameScreen {
	Logo,
	Title,
	Gameplay,
	Ending,
}

// Program main entry point
fn main() {
	// - Initialization -
	const SCREEN_WIDTH: i32 = 800;
	const SCREEN_HEIGHT: i32 = 450;

	let font = rl::Font::BUILTIN;

	rl::start_graphics!(
		SCREEN_WIDTH,
		SCREEN_HEIGHT,
		"raylib [core] example - basic screen manager",
	);

	let mut current_screen = GameScreen::Logo;

	// TODO: Initialize all required variables and load all required data here!

	let mut frames_counter = 0; // Useful to count frames

	rl::set_target_fps(60); // Set desired framerate (frames-per-second)

	// ------------------

	// Main game loop
	// Detect window close button or ESC key
	while !rl::window_should_close() {
		// - Update -
		match current_screen {
			GameScreen::Logo => {
				// TODO: Update LOGO screen variables here!

				frames_counter += 1; // Count frames
					 // Wait for 2 seconds (120 frames) before jumping to TITLE screen
				if frames_counter > 120 {
					current_screen = GameScreen::Title;
				}
			}
			GameScreen::Title => {
				// TODO: Update TITLE screen variables here!

				// Press enter to change to GAMEPLAY screen
				if rl::Key::Enter.is_pressed() || rl::Gesture::Tap.detected() {
					current_screen = GameScreen::Gameplay;
				}
			}
			GameScreen::Gameplay => {
				// TODO: Update GAMEPLAY screen variables here!

				// Press enter to change to ENDING screen
				if rl::Key::Enter.is_pressed() || rl::Gesture::Tap.detected() {
					current_screen = GameScreen::Ending;
				}
			}
			GameScreen::Ending => {
				// TODO: Update ENDING screen variables here!

				// Press enter to change to TITLE screen
				if rl::Key::Enter.is_pressed() || rl::Gesture::Tap.detected() {
					current_screen = GameScreen::Title;
				}
			}
		}

		// ----------

		// - Draw -
		rl::draw! {{
			rl::clear_background(rl::Color::RAY_WHITE);

			let screen_font = font
				.print()
				.pos(20, 20)
				.size(40.);

			let screen_rec = rl::Rec::from_size(SCREEN_WIDTH, SCREEN_HEIGHT).draw_with();

			match current_screen {
				GameScreen::Logo => {
					// TODO: Draw LOGO screen here!
					screen_font
						.tint(rl::Color::LIGHT_GRAY)
						.commit("LOGO SCREEN");

					font.print()
						.pos(290, 220)
						.size(20.)
						.tint(rl::Color::GRAY)
						.commit("WAIT for 2 SECONDS...");
				},
				GameScreen::Title => {
					// TODO: Draw TITLE screen here!
					screen_rec.tint(rl::Color::GREEN).commit();

					screen_font
						.tint(rl::Color::DARK_GREEN)
						.commit("TITLE SCREEN");

					font.print()
						.pos(120, 220)
						.size(20.)
						.tint(rl::Color::DARK_GREEN)
						.commit("PRESS ENTER or TAP to JUMP to GAMEPLAY SCREEN");
				},
				GameScreen::Gameplay => {
					// TODO: Draw GAMEPLAY screen here!
					screen_rec.tint(rl::Color::PURPLE).commit();

					screen_font
						.tint(rl::Color::MAROON)
						.commit("GAMEPLAY SCREEN");

					font.print()
						.pos(130, 220)
						.size(20.)
						.tint(rl::Color::MAROON)
						.commit("PRESS ENTER or TAP to JUMP to ENDING SCREEN");
				},
				GameScreen::Ending => {
					// TODO: Draw ENDING screen here!
					screen_rec.tint(rl::Color::BLUE).commit();

					screen_font
						.tint(rl::Color::DARK_BLUE)
						.commit("ENDING SCREEN");

					font.print()
						.pos(120, 220)
						.size(20.)
						.tint(rl::Color::DARK_BLUE)
						.commit("PRESS ENTER or TAP to RETURN to TITLE SCREEN");
				},
			}
		}}

		// --------
	}
}
