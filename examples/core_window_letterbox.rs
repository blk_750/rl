// Program main entry point
fn main() {
	// - Initialization -
	const WINDOW_WIDTH: i32 = 800;
	const WINDOW_HEIGHT: i32 = 450;

	let font = rl::Font::BUILTIN;

	// Enable config flags for resizable window and vertical synchro
	rl::set_window_resizable(true);
	rl::set_vsync_hint(true);

	rl::start_graphics!(
		WINDOW_WIDTH,
		WINDOW_HEIGHT,
		"raylib [core] example - window scale letterbox",
	);

	rl::set_window_minimum_size(320, 240);

	let game_screen_width = 640;
	let game_screen_height = 480;

	// Render texture initialization, used to hold the rendering result so we can easily resize it
	let mut target = rl::RenderTex::load(game_screen_width, game_screen_height);
	target.tex().set_filter(rl::TexFilter::Bilinear); // Texture scale filter to use

	let mut colors = [rl::Color::WHITE; 10];
	for i in 0..10 {
		colors[i] = rl::Color {
			r: rl::get_random_value(100, 250) as u8,
			g: rl::get_random_value(50, 150) as u8,
			b: rl::get_random_value(10, 100) as u8,
			a: 255,
		};
	}

	rl::set_target_fps(60); // Set our game to run at 60 frames-per-second

	// ------------------

	// Main game loop
	// Detect window close button or ESC key
	while !rl::window_should_close() {
		// - Update -
		// Compute required framebuffer scaling
		let scale = f32::min(
			rl::get_screen_width() as f32 / game_screen_width as f32,
			rl::get_screen_height() as f32 / game_screen_height as f32,
		);

		if rl::Key::Space.is_pressed() {
			// Recalculate random colors for the bars
			for i in 0..10 {
				colors[i] = rl::Color {
					r: rl::get_random_value(100, 250) as u8,
					g: rl::get_random_value(50, 150) as u8,
					b: rl::get_random_value(10, 100) as u8,
					a: 255,
				};
			}
		}

		// Update virtual mouse (clamped mouse value behind game screen)
		let mouse = rl::get_mouse_position();
		let virtual_mouse = rl::Vec2 {
			x: (mouse.x
				- (rl::get_screen_width() as f32 - (game_screen_width as f32 * scale)) * 0.5)
				/ scale,
			y: (mouse.y
				- (rl::get_screen_height() as f32 - (game_screen_height as f32 * scale)) * 0.5)
				/ scale,
		}
		.clamp(
			rl::Vec2 { x: 0., y: 0. },
			rl::Vec2 {
				x: rl::get_screen_width() as f32,
				y: rl::get_screen_height() as f32,
			},
		);

		// Apply the same transformation as the virtual mouse to the real mouse (i.e. to work with raygui)
		//rl::mouse_offset_set(
		//	(-(rl::get_screen_width() as f32 - (game_screen_width as f32 * scale)) * 0.5) as i32,
		//	(-(rl::get_screen_height() as f32 - (game_screen_height as f32 * scale)) * 0.5) as i32,
		//);
		//rl::mouse_scale_set(1./scale, 1./scale);

		// ----------

		// - Draw -
		// Draw everything in the render texture, note this will not be rendered on screen, yet
		rl::draw_onto! { &target => {
		rl::clear_background(rl::Color::RAY_WHITE); // Clear render texture background color

		for (i, _) in colors.iter().enumerate() {
			rl::Rec {
				x: 0.,
				y: ((game_screen_height / 10) * i as i32) as f32,
				width: game_screen_width as f32,
				height: (game_screen_height / 10) as f32,
			}
				.draw_with()
				.tint(colors[i])
				.commit();
		}

		font.print()
			.pos(10, 25)
			.size(20.)
			.commit("If executed inside a window,\nyou can resize the window,\nand see the screen scaling!");

		font.print()
			.pos(350, 25)
			.size(20.)
			.tint(rl::Color::GREEN)
			.commit(&format!("Default Mouse: [{} , {}]", mouse.x as i32, mouse.y as i32));

		font.print()
			.pos(350, 55)
			.size(20.)
			.tint(rl::Color::YELLOW)
			.commit(&format!("Virtual Mouse: [{} , {}]", virtual_mouse.x as i32, virtual_mouse.y as i32));
		} };

		rl::draw! {{
			rl::clear_background(rl::Color::BLACK); // Clear screen background

			// Draw render texture to screen, properly scaled
			target.tex().draw_with()
				.portion_size(target.tex().width(), target.tex().height() * -1)
				.pos_x_float((rl::get_screen_width() as f32 - (game_screen_width as f32 * scale)) * 0.5)
				.pos_y_float((rl::get_screen_height() as f32 - (game_screen_height as f32 * scale)) * 0.5)
				.width_float(game_screen_width as f32 * scale)
				.height_float(game_screen_height as f32 * scale)
				.commit();

		}}

		// --------
	}
}
