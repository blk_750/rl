// Program main entry point
fn main() {
	// - Initialization -
	const SCREEN_WIDTH: i32 = 800;
	const SCREEN_HEIGHT: i32 = 450;

	const SCROLL_SPEED: i32 = 4; // Scrolling speed in pixels

	let font = rl::Font::BUILTIN;

	rl::start_graphics!(
		SCREEN_WIDTH,
		SCREEN_HEIGHT,
		"raylib [core] example - input mouse wheel",
	);

	let mut box_pos_y = (SCREEN_HEIGHT / 2 - 40) as f32;

	rl::set_target_fps(60); // Set our game to run at 60 frames-per-second

	// ------------------

	// Main game loop
	// Detect window close button or ESC key
	while !rl::window_should_close() {
		// - Update -
		box_pos_y -= rl::get_mouse_wheel_move() * SCROLL_SPEED as f32;
		// ----------

		// - Draw -
		rl::draw! {{
			rl::clear_background(rl::Color::RAY_WHITE);

			rl::Rec {
				x: SCREEN_WIDTH as f32 / 2. - 40.,
				y: box_pos_y,
				width: 80.,
				height: 80.,
			}
				.draw_with()
				.tint(rl::Color::MAROON)
				.commit();

			font.print()
				.pos(10, 10)
				.size(20.)
				.tint(rl::Color::GRAY)
				.commit("Use mouse wheel to move the cube up and down!");

			font.print()
				.pos(10, 40)
				.size(20.)
				.tint(rl::Color::LIGHT_GRAY)
				.commit(&format!("Box position Y: {}", box_pos_y as i32));
		}}

		// --------
	}
}
