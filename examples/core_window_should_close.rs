// Program main entry point
fn main() {
	// - Initialization -
	const SCREEN_WIDTH: i32 = 800;
	const SCREEN_HEIGHT: i32 = 450;

	rl::start_graphics!(
		SCREEN_WIDTH,
		SCREEN_HEIGHT,
		"raylib [core] example - window should close",
	);

	let font = rl::Font::BUILTIN;

	rl::unbind_exit_key(); // Disable rl::Key::Escape to close window, X-button still works

	let mut exit_window_requested = false; // Flag to request window to exit

	rl::set_target_fps(60); // Set our game to run at 60 frames-per-second

	// ------------------

	// Main game loop
	loop {
		// - Update -
		// Detect if X-button or KEY_ESCAPE have been pressed to close window
		if rl::window_should_close() || rl::Key::Escape.is_pressed() {
			exit_window_requested = true;
		}

		if exit_window_requested {
			// A request for close window has been issued, we can save data before closing
			// or just show a message asking for confirmation

			if rl::Key::Y.is_pressed() {
				return;
			} else if rl::Key::N.is_pressed() {
				exit_window_requested = false;
			}
		}

		// ----------

		// - Draw -
		rl::draw!(=>);
		rl::clear_background(rl::Color::RAY_WHITE);

		if exit_window_requested {
			rl::Rec {
				x: 0.,
				y: 100.,
				width: SCREEN_WIDTH as f32,
				height: 200.,
			}
			.draw_with()
			.tint(rl::Color::BLACK)
			.commit();

			font.print()
				.pos(40, 180)
				.size(30.)
				.commit("Are you sure you want to exit program? [Y/N]");
		} else {
			font.print()
				.pos(120, 200)
				.size(20.)
				.tint(rl::Color::LIGHT_GRAY)
				.commit("Try to close the window to get confirmation message!");
		}

		// --------
	}
}
