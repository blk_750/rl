const G: f32 = 400.;
const PLAYER_JUMP_SPD: f32 = 350.;
const PLAYER_HOR_SPD: f32 = 200.;

struct Player {
	pos: rl::Vec2,
	speed: f32,
	can_jump: bool,
}

impl Player {
	fn update(&mut self, env_items: &[EnvItem], delta: f64) {
		if rl::Key::Left.is_down() {
			self.pos.x -= PLAYER_HOR_SPD * delta as f32;
		}

		if rl::Key::Right.is_down() {
			self.pos.x += PLAYER_HOR_SPD * delta as f32;
		}

		if rl::Key::Space.is_down() && self.can_jump {
			self.speed = -PLAYER_JUMP_SPD;
			self.can_jump = false;
		}

		let mut hit_obstacle = false;

		for item in env_items.iter() {
			let pl = self.pos;

			if item.blocking
				&& item.rec.x <= pl.x
				&& item.rec.x + item.rec.width >= pl.x
				&& item.rec.y >= pl.y
				&& item.rec.y <= pl.y + self.speed * delta as f32
			{
				hit_obstacle = true;
				self.speed = 0.;
				self.pos.y = item.rec.y;
			}
		}

		if hit_obstacle {
			self.can_jump = true;
		} else {
			self.pos.y += self.speed * delta as f32;
			self.speed += G * delta as f32;
			self.can_jump = false;
		}
	}
}

struct EnvItem {
	rec: rl::Rec,
	blocking: bool,
	color: rl::Color,
}

trait CameraSystem {
	fn update(
		&mut self,
		camera: &mut rl::Cam2D,
		player: &Player,
		env_items: &[EnvItem],
		delta: f64,
		width: i32,
		height: i32,
	);

	fn description(&self) -> &str;
}

struct UpdateCameraCenter();

impl CameraSystem for UpdateCameraCenter {
	fn update(
		&mut self,
		camera: &mut rl::Cam2D,
		player: &Player,
		_: &[EnvItem],
		_: f64,
		width: i32,
		height: i32,
	) {
		camera.offset = rl::Vec2 {
			x: width as f32 / 2.,
			y: height as f32 / 2.,
		};

		camera.target = player.pos;
	}

	fn description(&self) -> &str {
		"Follow player center"
	}
}

struct UpdateCameraCenterInsideMap();

impl CameraSystem for UpdateCameraCenterInsideMap {
	fn update(
		&mut self,
		camera: &mut rl::Cam2D,
		player: &Player,
		env_items: &[EnvItem],
		_: f64,
		width: i32,
		height: i32,
	) {
		let width = width as f32;
		let height = height as f32;

		camera.offset = rl::Vec2 {
			x: width / 2.,
			y: height / 2.,
		};

		camera.target = player.pos;

		let mut min_x: f32 = 1000.;
		let mut min_y: f32 = 1000.;
		let mut max_x: f32 = -1000.;
		let mut max_y: f32 = -1000.;

		for item in env_items {
			min_x = min_x.min(item.rec.x);
			max_x = max_x.max(item.rec.x + item.rec.width);
			min_y = min_y.min(item.rec.y);
			max_y = max_y.max(item.rec.y + item.rec.height);
		}

		let max = camera.to_screen(rl::Vec2 { x: max_x, y: max_y });

		let min = camera.to_screen(rl::Vec2 { x: min_x, y: min_y });

		if max.x < width {
			camera.offset.x = width - (max.x - width / 2.);
		}

		if max.y < height {
			camera.offset.y = height - (max.y - height / 2.);
		}

		if min.x > 0. {
			camera.offset.x = width / 2. - min.x;
		}

		if min.y > 0. {
			camera.offset.y = height / 2. - min.y;
		}
	}

	fn description(&self) -> &str {
		"Follow player center, but clamp to map edges"
	}
}

struct UpdateCameraCenterSmoothFollow();

impl CameraSystem for UpdateCameraCenterSmoothFollow {
	fn update(
		&mut self,
		camera: &mut rl::Cam2D,
		player: &Player,
		_: &[EnvItem],
		delta: f64,
		width: i32,
		height: i32,
	) {
		const MIN_SPEED: f32 = 30.;
		const MIN_EFFECT_LEN: f32 = 10.;
		const FRACTION_SPEED: f32 = 0.8;

		camera.offset = rl::Vec2 {
			x: width as f32 / 2.,
			y: height as f32 / 2.,
		};

		let diff = player.pos - camera.target;
		let len = diff.length();

		if len > MIN_EFFECT_LEN {
			let speed = f32::max(FRACTION_SPEED * len, MIN_SPEED);
			camera.target += diff.scale(speed * delta as f32 / len);
		}
	}

	fn description(&self) -> &str {
		"Follow player center; smoothed"
	}
}

struct UpdateCameraEvenOutOnLanding {
	even_out_target: Option<f32>,
}

impl UpdateCameraEvenOutOnLanding {
	fn new() -> Self {
		Self {
			even_out_target: None,
		}
	}
}

impl CameraSystem for UpdateCameraEvenOutOnLanding {
	fn update(
		&mut self,
		camera: &mut rl::Cam2D,
		player: &Player,
		_: &[EnvItem],
		delta: f64,
		width: i32,
		height: i32,
	) {
		const EVEN_OUT_SPEED: f32 = 700.;

		camera.offset = rl::Vec2 {
			x: width as f32 / 2.,
			y: height as f32 / 2.,
		};

		camera.target.x = player.pos.x;

		if let Some(even_out_target) = self.even_out_target {
			if even_out_target > camera.target.y {
				camera.target.y += EVEN_OUT_SPEED * delta as f32;

				if camera.target.y > even_out_target {
					camera.target.y = even_out_target;
					self.even_out_target = None;
				}
			} else {
				camera.target.y -= EVEN_OUT_SPEED * delta as f32;

				if camera.target.y < EVEN_OUT_SPEED * delta as f32 {
					camera.target.y = even_out_target;
					self.even_out_target = None;
				}
			}
		} else {
			if player.can_jump && player.speed == 0. && player.pos.y != camera.target.y {
				self.even_out_target = Some(player.pos.y);
			}
		}
	}

	fn description(&self) -> &str {
		"Follow player center horizontally; update player center vertically after landing"
	}
}

struct UpdateCameraPlayerBoundsPush();

impl CameraSystem for UpdateCameraPlayerBoundsPush {
	fn update(
		&mut self,
		camera: &mut rl::Cam2D,
		player: &Player,
		_: &[EnvItem],
		_: f64,
		width: i32,
		height: i32,
	) {
		let width = width as f32;
		let height = height as f32;

		const BBOX: rl::Vec2 = rl::Vec2 { x: 0.2, y: 0.2 };

		let bbox_world_min = camera.to_world(rl::Vec2 {
			x: (1. - BBOX.x) * 0.5 * width,
			y: (1. - BBOX.y) * 0.5 * height,
		});

		let bbox_world_max = camera.to_world(rl::Vec2 {
			x: (1. + BBOX.x) * 0.5 * width,
			y: (1. + BBOX.y) * 0.5 * height,
		});

		camera.offset = rl::Vec2 {
			x: (1. - BBOX.x) * 0.5 * width,
			y: (1. - BBOX.y) * 0.5 * height,
		};

		if player.pos.x < bbox_world_min.x {
			camera.target.x = player.pos.x;
		}

		if player.pos.y < bbox_world_min.y {
			camera.target.y = player.pos.y;
		}

		if player.pos.x > bbox_world_max.x {
			camera.target.x = bbox_world_min.x + (player.pos.x - bbox_world_max.x);
		}

		if player.pos.y > bbox_world_max.y {
			camera.target.y = bbox_world_min.y + (player.pos.y - bbox_world_max.y);
		}
	}

	fn description(&self) -> &str {
		"Player push camera on getting too close to screen edge"
	}
}

// Program main entry point
fn main() {
	// - Initialization -
	const SCREEN_WIDTH: i32 = 800;
	const SCREEN_HEIGHT: i32 = 450;

	let font = rl::Font::BUILTIN;

	rl::start_graphics!(
		SCREEN_WIDTH,
		SCREEN_HEIGHT,
		"raylib [core] example - 2d camera",
	);

	let mut player = Player {
		pos: rl::Vec2 { x: 400., y: 280. },
		speed: 0.,
		can_jump: false,
	};

	let env_items = [
		EnvItem {
			rec: rl::Rec {
				x: 0.,
				y: 0.,
				width: 1000.,
				height: 400.,
			},
			blocking: false,
			color: rl::Color::LIGHT_GRAY,
		},
		EnvItem {
			rec: rl::Rec {
				x: 0.,
				y: 400.,
				width: 1000.,
				height: 200.,
			},
			blocking: true,
			color: rl::Color::GRAY,
		},
		EnvItem {
			rec: rl::Rec {
				x: 300.,
				y: 200.,
				width: 400.,
				height: 10.,
			},
			blocking: true,
			color: rl::Color::GRAY,
		},
		EnvItem {
			rec: rl::Rec {
				x: 250.,
				y: 300.,
				width: 100.,
				height: 10.,
			},
			blocking: true,
			color: rl::Color::GRAY,
		},
		EnvItem {
			rec: rl::Rec {
				x: 650.,
				y: 300.,
				width: 100.,
				height: 10.,
			},
			blocking: true,
			color: rl::Color::GRAY,
		},
	];

	let mut camera = rl::Cam2D {
		target: player.pos,
		offset: rl::Vec2 {
			x: SCREEN_WIDTH as f32 / 2.,
			y: SCREEN_HEIGHT as f32 / 2.,
		},
		rotation: 0.,
		zoom: 1.,
	};

	// Store pointers to the multiple update camera functions
	let mut camera_modes: Vec<Box<dyn CameraSystem>> = vec![
		Box::new(UpdateCameraCenter()),
		Box::new(UpdateCameraCenterInsideMap()),
		Box::new(UpdateCameraCenterSmoothFollow()),
		Box::new(UpdateCameraEvenOutOnLanding::new()),
		Box::new(UpdateCameraPlayerBoundsPush()),
	];

	rl::set_target_fps(60);

	// ------------------

	// Main game loop
	while !rl::window_should_close() {
		// - Update -
		let delta_time = rl::get_frame_time();

		player.update(&env_items, delta_time);

		camera.zoom = (camera.zoom + rl::get_mouse_wheel_move() * 0.05)
			.min(3.)
			.max(0.25);

		if rl::Key::R.is_pressed() {
			camera.zoom = 1.;
			player.pos = rl::Vec2 { x: 400., y: 280. };
		}

		if rl::Key::C.is_pressed() {
			camera_modes.rotate_left(1);
		}

		// Call update camera function inside a vec
		camera_modes[0].update(
			&mut camera,
			&player,
			&env_items,
			delta_time,
			SCREEN_WIDTH,
			SCREEN_HEIGHT,
		);

		// ----------

		// - Draw -
		rl::draw! {{
			rl::clear_background(rl::Color::LIGHT_GRAY);

			rl::with_camera_2d! { camera => {
				for item in env_items.iter() {
					item.rec
						.draw_with()
						.tint(item.color)
						.commit();
				}

				rl::Rec {
					x: player.pos.x - 20.,
					y: player.pos.y - 40.,
					width: 40.,
					height: 40.,
				}
					.draw_with()
					.tint(rl::Color::RED)
					.commit();
			} }

			let font_header = font.print()
				.pos_x(20)
				.size(10.)
				.tint(rl::Color::BLACK);

			let font_body = font.print()
				.pos_x(40)
				.size(10.)
				.tint(rl::Color::DARK_GRAY);

			font_header.pos_y(20).commit("Controls:");
			font_body.pos_y(40).commit("- Right/Left to move");
			font_body.pos_y(60).commit("- Space to jump");
			font_body.pos_y(80).commit("- Mouse Wheel to Zoom in-out, R to reset zoom");
			font_body.pos_y(100).commit("- C to change camera mode");

			font_header.pos_y(120).commit("Current camera mode:");
			font_body.pos_y(140).commit(camera_modes[0].description());
		}}

		// --------
	}
}
