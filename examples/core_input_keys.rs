// Program main entry point
fn main() {
	// - Initialization -
	const SCREEN_WIDTH: i32 = 800;
	const SCREEN_HEIGHT: i32 = 450;

	let font = rl::Font::BUILTIN;

	rl::start_graphics!(
		SCREEN_WIDTH,
		SCREEN_HEIGHT,
		"raylib [core] example - keyboard input",
	);

	let mut ball_pos = rl::Vec2 {
		x: SCREEN_WIDTH as f32 / 2.,
		y: SCREEN_HEIGHT as f32 / 2.,
	};

	rl::set_target_fps(60); // Set our game to run at 60 frames-per-second

	// ------------------

	// Main game loop
	// Detect window close button or ESC key
	while !rl::window_should_close() {
		// - Update -
		if rl::Key::Right.is_down() {
			ball_pos.x += 2.;
		}
		if rl::Key::Left.is_down() {
			ball_pos.x -= 2.;
		}
		if rl::Key::Up.is_down() {
			ball_pos.y -= 2.;
		}
		if rl::Key::Down.is_down() {
			ball_pos.y += 2.;
		}
		// ----------

		// - Draw -
		rl::draw! {{
			rl::clear_background(rl::Color::RAY_WHITE);

			font.print()
				.pos(10, 10)
				.size(20.)
				.tint(rl::Color::DARK_GRAY)
				.commit("move the ball with arrow keys");

			ball_pos
				.circle(50.)
				.draw()
				.tint(rl::Color::MAROON)
				.commit();
		}}

		// --------
	}
}
