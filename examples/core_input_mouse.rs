// Program main entry point
fn main() {
	// - Initialization -
	const SCREEN_WIDTH: i32 = 800;
	const SCREEN_HEIGHT: i32 = 450;

	let font = rl::Font::BUILTIN;

	rl::start_graphics!(
		SCREEN_WIDTH,
		SCREEN_HEIGHT,
		"raylib [core] example - mouse input",
	);

	let mut ball_pos;

	let mut ball_color = rl::Color::DARK_BLUE;

	rl::set_target_fps(60); // Set our game to run at 60 frames-per-second

	// ------------------

	// Main game loop
	// Detect window close button or ESC key
	while !rl::window_should_close() {
		// - Update -
		ball_pos = rl::get_mouse_position();

		ball_color = if rl::MB::Left.is_pressed() {
			rl::Color::MAROON
		} else if rl::MB::Middle.is_pressed() {
			rl::Color::LIME
		} else if rl::MB::Right.is_pressed() {
			rl::Color::DARK_BLUE
		} else if rl::MB::Side.is_pressed() {
			rl::Color::PURPLE
		} else if rl::MB::Extra.is_pressed() {
			rl::Color::YELLOW
		} else if rl::MB::Forward.is_pressed() {
			rl::Color::ORANGE
		} else if rl::MB::Back.is_pressed() {
			rl::Color::BEIGE
		} else {
			ball_color
		};
		// ----------

		// - Draw -
		rl::draw! {{
			rl::clear_background(rl::Color::RAY_WHITE);

			ball_pos
				.circle(50.)
				.draw()
				.tint(ball_color)
				.commit();

			font.print()
				.pos(10, 10)
				.size(20.)
				.tint(rl::Color::DARK_GRAY)
				.commit("move ball with mouse and click mouse button to change color");
		}}

		// --------
	}
}
