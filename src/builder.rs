use crate::*;

macro_rules! dst_impl {
	() => {
		pub fn size(self, width: i32, height: i32) -> Self {
			self.width(width).height(height)
		}

		pub fn width(self, width: i32) -> Self {
			self.width_float(width as f32)
		}

		pub fn height(self, height: i32) -> Self {
			self.height_float(height as f32)
		}

		pub fn width_float(self, width: f32) -> Self {
			Self {
				dst: Rec { width, ..self.dst },
				..self
			}
		}

		pub fn height_float(self, height: f32) -> Self {
			Self {
				dst: Rec { height, ..self.dst },
				..self
			}
		}

		pub fn size_and_pos_rec(self, rec: Rec) -> Self {
			Self { dst: rec, ..self }
		}

		pub fn pos(self, x: i32, y: i32) -> Self {
			self.pos_vec(Vec2 {
				x: x as f32,
				y: y as f32,
			})
		}

		pub fn pos_vec(self, pos: Vec2) -> Self {
			Self {
				dst: Rec {
					x: pos.x,
					y: pos.y,
					..self.dst
				},
				..self
			}
		}

		pub fn pos_x(self, x: i32) -> Self {
			self.pos_x_float(x as f32)
		}

		pub fn pos_y(self, y: i32) -> Self {
			self.pos_y_float(y as f32)
		}

		pub fn pos_x_float(self, x: f32) -> Self {
			Self {
				dst: Rec { x, ..self.dst },
				..self
			}
		}

		pub fn pos_y_float(self, y: f32) -> Self {
			Self {
				dst: Rec { y, ..self.dst },
				..self
			}
		}
	};
}

macro_rules! origin_impl {
	() => {
		pub fn origin_vec(self, origin: Vec2) -> Self {
			Self { origin, ..self }
		}
	};
}

macro_rules! rotation_impl {
	() => {
		pub fn rotation(self, rotation: f32) -> Self {
			Self { rotation, ..self }
		}
	};
}

macro_rules! tint_impl {
	() => {
		pub fn tint(self, tint: Color) -> Self {
			Self { tint, ..self }
		}
	};
}

macro_rules! pos_impl {
	() => {
		pub fn pos(self, x: i32, y: i32) -> Self {
			self.pos_vec(Vec2 {
				x: x as f32,
				y: y as f32,
			})
		}

		pub fn pos_vec(self, pos: Vec2) -> Self {
			Self { pos, ..self }
		}

		pub fn pos_x(self, x: i32) -> Self {
			Self {
				pos: Vec2 {
					x: x as f32,
					..self.pos
				},
				..self
			}
		}

		pub fn pos_y(self, y: i32) -> Self {
			Self {
				pos: Vec2 {
					y: y as f32,
					..self.pos
				},
				..self
			}
		}
	};
}

macro_rules! size_impl {
	() => {
		pub fn size(self, size: f32) -> Self {
			Self { size, ..self }
		}
	};
}

macro_rules! spacing_impl {
	() => {
		pub fn spacing(self, spacing: f32) -> Self {
			Self { spacing, ..self }
		}
	};
}

#[derive(Copy, Clone)]
pub struct Draw<'a> {
	tex: &'a Tex<'a>,
	flip_x: bool,
	flip_y: bool,
	src: Rec,
	dst: Rec,
	origin: Vec2,
	rotation: f32,
	tint: Color,
	scale_if_tiled: Option<f32>,
}

#[derive(Copy, Clone)]
pub struct DrawNPatch<'a> {
	tex: &'a Tex<'a>,
	npatch: &'a NPatch,
	dst: Rec,
	origin: Vec2,
	rotation: f32,
	tint: Color,
}

#[derive(Copy, Clone)]
pub struct Print<'a> {
	font: &'a Font,
	origin: Vec2,
	rotation: f32,
	tint: Color,
	pos: Vec2,
	size: f32,
	spacing: f32,
}

#[derive(Copy, Clone)]
pub struct PrintOnImage<'a> {
	font: &'a Font,
	origin: Vec2,
	tint: Color,
	pos: Vec2,
	size: f32,
	spacing: f32,
}

#[derive(Copy, Clone)]
pub struct DrawRec {
	rec: Rec,
	origin: Vec2,
	rotation: f32,
	tint: Color,
}

#[derive(Copy, Clone)]
pub struct DrawCircle {
	circle: Circle,
	tint: Color,
}

impl<'a> Draw<'a> {
	pub fn new(tex: &'a Tex<'a>) -> Self {
		Self {
			tex,
			dst: Rec {
				x: 0.,
				y: 0.,
				width: tex.ffi.width as f32,
				height: tex.ffi.height as f32,
			},
			origin: Vec2 { x: 0., y: 0. },
			rotation: 0.,
			tint: Color::WHITE,
			src: Rec {
				x: 0.,
				y: 0.,
				width: tex.ffi.width as f32,
				height: tex.ffi.height as f32,
			},
			scale_if_tiled: None,
			flip_x: false,
			flip_y: false,
		}
	}

	pub fn commit(self) {
		let src = Rec {
			width: if self.flip_x {
				self.src.width * -1.
			} else {
				self.src.width
			},
			height: if self.flip_y {
				self.src.height * -1.
			} else {
				self.src.height
			},
			..self.src
		};

		unsafe {
			if let Some(scale) = self.scale_if_tiled {
				ffi::DrawTextureTiled(
					self.tex.ffi,
					src.ffi(),
					self.dst.ffi(),
					self.origin.ffi(),
					self.rotation,
					scale,
					self.tint.ffi(),
				)
			} else {
				ffi::DrawTexturePro(
					self.tex.ffi,
					src.ffi(),
					self.dst.ffi(),
					self.origin.ffi(),
					self.rotation,
					self.tint.ffi(),
				)
			}
		}
	}

	pub fn portion_rec(self, portion: Rec) -> Self {
		Self {
			src: portion,
			..self
		}
	}

	pub fn portion_size(self, width: i32, height: i32) -> Self {
		self.portion_width(width).portion_height(height)
	}

	pub fn portion_width(self, width: i32) -> Self {
		Self {
			src: Rec {
				width: width as f32,
				..self.src
			},
			..self
		}
	}

	pub fn portion_height(self, height: i32) -> Self {
		Self {
			src: Rec {
				height: height as f32,
				..self.src
			},
			..self
		}
	}

	pub fn tile(self, scale: f32) -> Self {
		Self {
			scale_if_tiled: Some(scale),
			..self
		}
	}

	pub fn flipped_x(self) -> Self {
		Self {
			flip_x: true,
			..self
		}
	}

	pub fn flipped_y(self) -> Self {
		Self {
			flip_y: true,
			..self
		}
	}

	dst_impl!();
	origin_impl!();
	rotation_impl!();
	tint_impl!();
}

impl<'a> DrawNPatch<'a> {
	pub fn new(tex: &'a Tex<'a>, npatch: &'a NPatch) -> Self {
		Self {
			tex,
			dst: Rec {
				x: 0.,
				y: 0.,
				width: tex.ffi.width as f32,
				height: tex.ffi.height as f32,
			},
			origin: Vec2 { x: 0., y: 0. },
			rotation: 0.,
			tint: Color::WHITE,
			npatch: &npatch,
		}
	}

	pub fn commit(self) {
		let npatch = ffi::NPatchInfo {
			source: self.npatch.src.ffi(),
			left: self.npatch.left,
			top: self.npatch.top,
			right: self.npatch.right,
			bottom: self.npatch.bottom,
			layout: match self.npatch.layout {
				NPatchType::Nine => ffi::NPatchLayout_NPATCH_NINE_PATCH,
				NPatchType::Vertical => ffi::NPatchLayout_NPATCH_THREE_PATCH_VERTICAL,
				NPatchType::Horizontal => ffi::NPatchLayout_NPATCH_THREE_PATCH_HORIZONTAL,
			} as i32,
		};

		unsafe {
			ffi::DrawTextureNPatch(
				self.tex.ffi,
				npatch,
				self.dst.ffi(),
				self.origin.ffi(),
				self.rotation,
				self.tint.ffi(),
			)
		}
	}

	dst_impl!();
	origin_impl!();
	rotation_impl!();
	tint_impl!();
}

impl<'a> Print<'a> {
	pub fn new(font: &'a Font) -> Self {
		Self {
			origin: Vec2 { x: 0., y: 0. },
			rotation: 0.,
			tint: Color::WHITE,
			font: &font,
			pos: Vec2 { x: 0., y: 0. },
			size: font.ffi().baseSize as f32,
			spacing: 0.,
		}
	}

	pub fn commit(self, s: &str) {
		let s = CString::new(s).unwrap();

		let spacing = if self.font.0.is_none() {
			self.spacing + 2.
		} else {
			self.spacing
		};

		unsafe {
			ffi::DrawTextPro(
				self.font.ffi(),
				s.as_ptr() as *const i8,
				self.pos.ffi(),
				self.origin.ffi(),
				self.rotation,
				self.size,
				spacing,
				self.tint.ffi(),
			)
		}
	}

	pub fn centered_origin(self, s: &str) -> Self {
		let dim = self.font.measure_text(s, self.size, self.spacing);
		Self {
			origin: Vec2 {
				x: dim.x / 2.,
				y: dim.y / 2.,
			},
			..self
		}
	}

	pub fn floored_origin(self) -> Self {
		Self {
			origin: self.origin.floored(),
			..self
		}
	}

	origin_impl!();
	rotation_impl!();
	tint_impl!();
	pos_impl!();
	size_impl!();
	spacing_impl!();
}

impl<'a> PrintOnImage<'a> {
	pub fn new(font: &'a Font) -> Self {
		Self {
			origin: Vec2 { x: 0., y: 0. },
			tint: Color::WHITE,
			font: &font,
			pos: Vec2 { x: 0., y: 0. },
			size: font.ffi().baseSize as f32,
			spacing: 0.,
		}
	}

	pub fn commit(self, s: &str, img: &mut Image) {
		let s = CString::new(s).unwrap();
		let pos = Vec2 {
			x: self.pos.x + self.origin.x,
			y: self.pos.y + self.origin.y,
		};

		let spacing = if self.font.0.is_none() {
			self.spacing + 2.
		} else {
			self.spacing
		};

		unsafe {
			ffi::ImageDrawTextEx(
				&mut img.0 as *mut ffi::Image,
				self.font.ffi(),
				s.as_ptr() as *const i8,
				pos.ffi(),
				self.size,
				spacing,
				self.tint.ffi(),
			)
		}
	}

	origin_impl!();
	tint_impl!();
	pos_impl!();
	size_impl!();
	spacing_impl!();
}

impl DrawRec {
	pub fn new(rec: Rec) -> Self {
		Self {
			rec,
			origin: Vec2 { x: 0., y: 0. },
			rotation: 0.,
			tint: Color::WHITE,
		}
	}

	pub fn commit(self) {
		unsafe {
			ffi::DrawRectanglePro(
				self.rec.ffi(),
				self.origin.ffi(),
				self.rotation,
				self.tint.ffi(),
			)
		}
	}

	origin_impl!();
	rotation_impl!();
	tint_impl!();
}

impl DrawCircle {
	pub fn new(circle: Circle) -> Self {
		Self {
			circle,
			tint: Color::WHITE,
		}
	}

	pub fn commit(self) {
		unsafe {
			ffi::DrawCircleV(
				self.circle.center.ffi(),
				self.circle.radius,
				self.tint.ffi(),
			)
		}
	}

	tint_impl!();
}
