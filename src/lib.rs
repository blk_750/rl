use raylib_ffi as ffi;
use std::ffi::CString;
use std::marker::PhantomData;
use std::ops::Drop;

pub mod builder;

pub struct Tex<'a> {
	ffi: ffi::Texture,
	_phantom: Option<PhantomData<&'a ()>>,
}

impl Tex<'_> {
	pub fn load(path: &str) -> Self {
		unsafe {
			let path = CString::new(path).unwrap();
			Self {
				ffi: ffi::LoadTexture(path.as_ptr() as *const i8),
				_phantom: None,
			}
		}
	}

	pub fn draw_with<'a>(&'a self) -> builder::Draw<'a> {
		builder::Draw::new(self)
	}

	pub fn draw_npatch_with<'a>(&'a self, npatch: &'a NPatch) -> builder::DrawNPatch<'a> {
		builder::DrawNPatch::new(self, &npatch)
	}

	pub fn width(&self) -> i32 {
		self.ffi.width
	}

	pub fn height(&self) -> i32 {
		self.ffi.height
	}

	pub fn into_image(&self) -> Image {
		Image(unsafe { ffi::LoadImageFromTexture(self.ffi) })
	}

	pub fn set_wrap_clamp(&mut self) {
		unsafe { ffi::SetTextureWrap(self.ffi, ffi::TextureWrap_TEXTURE_WRAP_CLAMP as i32) }
	}

	pub fn set_filter(&mut self, filter: TexFilter) {
		unsafe { ffi::SetTextureFilter(self.ffi, filter.ffi()) }
	}

	pub fn from_image(img: Image) -> Self {
		unsafe {
			Self {
				ffi: ffi::LoadTextureFromImage(img.0),
				_phantom: None,
			}
		}
	}
}

impl Drop for Tex<'_> {
	fn drop(&mut self) {
		if self._phantom.is_none() {
			unsafe { ffi::UnloadTexture(self.ffi) }
		}
	}
}

pub struct RenderTex(ffi::RenderTexture);

impl RenderTex {
	pub fn load(w: i32, h: i32) -> Self {
		unsafe { Self(ffi::LoadRenderTexture(w, h)) }
	}

	pub fn tex<'a>(&self) -> Tex<'a> {
		Tex {
			ffi: self.0.texture,
			_phantom: Some(PhantomData),
		}
	}
}

impl Drop for RenderTex {
	fn drop(&mut self) {
		unsafe { ffi::UnloadRenderTexture(self.0) }
	}
}

pub struct Image(ffi::Image);

impl Image {
	pub fn load(path: &str) -> Self {
		Self(unsafe {
			let path = CString::new(path).unwrap();
			ffi::LoadImage(path.as_ptr() as *const i8)
		})
	}

	pub fn load_from_mem(data: Vec<u8>, extension: &str) -> Self {
		Self(unsafe {
			let file_type = std::ffi::CString::new(extension).unwrap();
			ffi::LoadImageFromMemory(file_type.as_ptr(), data.as_ptr(), data.len() as i32)
		})
	}

	pub fn from_color(w: i32, h: i32, c: Color) -> Self {
		Self(unsafe { ffi::GenImageColor(w, h, c.ffi()) })
	}

	pub fn get_color(&self, x: i32, y: i32) -> Color {
		Color::native(unsafe { ffi::GetImageColor(self.0, x, y) })
	}

	pub fn set_color(&mut self, x: i32, y: i32, c: Color) {
		unsafe { ffi::ImageDrawPixel(&mut self.0 as *mut ffi::Image, x, y, c.ffi()) }
	}

	pub fn draw(&mut self, src: &Self, src_rect: Rec, dst_rect: Rec, tint: Color) {
		unsafe {
			ffi::ImageDraw(
				&mut self.0 as *mut ffi::Image,
				src.0,
				src_rect.ffi(),
				dst_rect.ffi(),
				tint.ffi(),
			)
		}
	}

	pub fn draw_line(&mut self, start: Vec2, end: Vec2, c: Color) {
		unsafe {
			ffi::ImageDrawLineV(
				&mut self.0 as *mut ffi::Image,
				start.ffi(),
				end.ffi(),
				c.ffi(),
			)
		}
	}

	pub fn draw_image(&mut self, src: &Image, src_rec: Rec, dst_rec: Rec, tint: Color) {
		unsafe {
			ffi::ImageDraw(
				&mut self.0 as *mut ffi::Image,
				src.0,
				src_rec.ffi(),
				dst_rec.ffi(),
				tint.ffi(),
			)
		}
	}

	#[deprecated]
	pub fn draw_text_ex(
		&mut self,
		font: &Font,
		text: &str,
		pos: Vec2,
		size: f32,
		spacing: f32,
		tint: Color,
	) {
		font.print_on_image()
			.pos_vec(pos)
			.size(size)
			.spacing(spacing)
			.tint(tint)
			.commit(text, self);
	}

	pub fn alpha_crop(&mut self, threshold: f32) {
		unsafe { ffi::ImageAlphaCrop(&mut self.0 as *mut ffi::Image, threshold) }
	}

	pub fn alpha_mask(&mut self, mask: &Image) {
		unsafe { ffi::ImageAlphaMask(&mut self.0 as *mut ffi::Image, mask.0) }
	}

	pub fn resize_nn(&mut self, width: i32, height: i32) {
		unsafe { ffi::ImageResizeNN(&mut self.0 as *mut ffi::Image, width, height) }
	}

	pub fn export(&self, path: &str) {
		unsafe {
			let path = CString::new(path).unwrap();
			ffi::ExportImage(self.0, path.as_ptr() as *const i8);
		}
	}

	pub fn width(&self) -> i32 {
		self.0.width
	}

	pub fn height(&self) -> i32 {
		self.0.height
	}
}

impl Clone for Image {
	fn clone(&self) -> Self {
		Self(unsafe { ffi::ImageCopy((*self).0) })
	}
}

impl Drop for Image {
	fn drop(&mut self) {
		unsafe { ffi::UnloadImage(self.0) }
	}
}

pub struct Shader(ffi::Shader);

impl Shader {
	pub fn load_str(vs: Option<&str>, fs: Option<&str>) -> Self {
		//XXX
		unsafe {
			match (vs, fs) {
				(Some(vs), Some(fs)) => {
					let vs = CString::new(vs).unwrap();
					let fs = CString::new(fs).unwrap();

					Self(ffi::LoadShaderFromMemory(
						vs.as_ptr() as *const i8,
						fs.as_ptr() as *const i8,
					))
				}
				(Some(vs), None) => {
					let vs = CString::new(vs).unwrap();

					Self(ffi::LoadShaderFromMemory(
						vs.as_ptr() as *const i8,
						std::ptr::null() as *const i8,
					))
				}
				(None, Some(fs)) => {
					let fs = CString::new(fs).unwrap();

					Self(ffi::LoadShaderFromMemory(
						std::ptr::null() as *const i8,
						fs.as_ptr() as *const i8,
					))
				}
				(None, None) => Self(ffi::LoadShaderFromMemory(
					std::ptr::null() as *const i8,
					std::ptr::null() as *const i8,
				)),
			}
		}
	}

	pub fn get_location(&self, uniform: &str) -> i32 {
		let uniform = CString::new(uniform).unwrap();
		unsafe { ffi::GetShaderLocation(self.0, uniform.as_ptr() as *const i8) }
	}

	pub fn set_vec2(&self, loc: i32, v: Vec2) {
		unsafe {
			ffi::SetShaderValue(
				self.0,
				loc,
				&v as *const Vec2 as *const libc::c_void,
				ffi::ShaderUniformDataType_SHADER_UNIFORM_VEC2 as i32,
			)
		}
	}

	pub fn set_tex(&self, loc: i32, tex: &Tex) {
		unsafe { ffi::SetShaderValueTexture(self.0, loc, tex.ffi) }
	}

	pub fn set_render_tex(&self, loc: i32, rt: &RenderTex) {
		unsafe { ffi::SetShaderValueTexture(self.0, loc, rt.0.texture) }
	}

	pub fn begin(&self) {
		unsafe { ffi::BeginShaderMode(self.0) }
	}

	pub fn end() {
		unsafe { ffi::EndShaderMode() }
	}
}

impl Drop for Shader {
	fn drop(&mut self) {
		unsafe { ffi::UnloadShader(self.0) }
	}
}

pub struct Sound(ffi::Sound);

impl Sound {
	pub fn load(path: &str) -> Self {
		let path = CString::new(path).unwrap();
		Sound(unsafe { ffi::LoadSound(path.as_ptr() as *const i8) })
	}

	pub fn play(&self) {
		unsafe { ffi::PlaySound(self.0) }
	}
}

impl Drop for Sound {
	fn drop(&mut self) {
		unsafe { ffi::UnloadSound(self.0) }
	}
}

#[derive(Copy, Clone)]
pub struct Rec {
	pub x: f32,
	pub y: f32,
	pub width: f32,
	pub height: f32,
}

impl Rec {
	pub fn new(x: i32, y: i32, width: i32, height: i32) -> Self {
		Self {
			x: x as f32,
			y: y as f32,
			width: width as f32,
			height: height as f32,
		}
	}

	pub fn from_size(width: i32, height: i32) -> Self {
		Self {
			x: 0.,
			y: 0.,
			width: width as f32,
			height: height as f32,
		}
	}

	pub fn center(self) -> Vec2 {
		Vec2 {
			x: (self.width / 2.) + self.x,
			y: (self.height / 2.) + self.y,
		}
	}

	pub fn draw_with(self) -> builder::DrawRec {
		builder::DrawRec::new(self)
	}

	pub fn collides(self, rec: Self) -> bool {
		unsafe { ffi::CheckCollisionRecs(self.ffi(), rec.ffi()) }
	}

	pub(crate) fn ffi(self) -> ffi::Rectangle {
		ffi::Rectangle {
			x: self.x,
			y: self.y,
			width: self.width,
			height: self.height,
		}
	}

	pub(crate) fn native(rec: ffi::Rectangle) -> Self {
		Self {
			x: rec.x,
			y: rec.y,
			width: rec.width,
			height: rec.height,
		}
	}
}

#[derive(Copy, Clone, PartialEq, Debug)]
pub struct Vec2 {
	pub x: f32,
	pub y: f32,
}

impl Vec2 {
	pub fn new() -> Self {
		Self { x: 0., y: 0. }
	}

	pub fn circle(self, radius: f32) -> Circle {
		Circle {
			center: self,
			radius,
		}
	}

	pub fn scale(self, by: f32) -> Self {
		Self {
			x: self.x * by,
			y: self.y * by,
		}
	}

	pub fn center(self) -> Self {
		Self {
			x: self.x / 2.,
			y: self.y / 2.,
		}
	}

	pub fn length(self) -> f32 {
		((self.x * self.x) + (self.y * self.y)).sqrt()
	}

	pub fn floored(self) -> Self {
		Self {
			x: self.x.floor(),
			y: self.y.floor(),
		}
	}

	pub fn clamp(self, min_vec: Self, max_vec: Self) -> Self {
		Self {
			x: f32::min(max_vec.x, f32::max(min_vec.x, self.x)),
			y: f32::min(max_vec.y, f32::max(min_vec.y, self.y)),
		}
	}

	pub(crate) fn ffi(self) -> ffi::Vector2 {
		ffi::Vector2 {
			x: self.x,
			y: self.y,
		}
	}

	pub(crate) fn native(vec: ffi::Vector2) -> Self {
		Self { x: vec.x, y: vec.y }
	}
}

impl std::ops::Add for Vec2 {
	type Output = Self;

	fn add(self, other: Self) -> Self {
		Self {
			x: self.x + other.x,
			y: self.y + other.y,
		}
	}
}

impl std::ops::AddAssign for Vec2 {
	fn add_assign(&mut self, other: Self) {
		*self = *self + other;
	}
}

impl std::ops::Sub for Vec2 {
	type Output = Self;

	fn sub(self, other: Self) -> Self {
		Self {
			x: self.x - other.x,
			y: self.y - other.y,
		}
	}
}

impl std::ops::SubAssign for Vec2 {
	fn sub_assign(&mut self, other: Self) {
		*self = *self - other;
	}
}

pub struct Font(Option<ffi::Font>);

impl Font {
	pub const BUILTIN: Font = Font(None);

	pub fn print<'a>(&'a self) -> builder::Print<'a> {
		builder::Print::new(self)
	}

	pub fn print_on_image<'a>(&'a self) -> builder::PrintOnImage<'a> {
		builder::PrintOnImage::new(self)
	}

	pub fn load_from_mem_simple(data: Vec<u8>, extension: &str, size: i32) -> Self {
		unsafe {
			let file_type = std::ffi::CString::new(extension).unwrap();
			Self(Some(ffi::LoadFontFromMemory(
				file_type.as_ptr(),
				data.as_ptr(),
				data.len() as i32,
				size,
				std::ptr::null_mut(),
				0,
			)))
		}
	}

	pub fn measure_text(&self, s: &str, size: f32, spacing: f32) -> Vec2 {
		let s = std::ffi::CString::new(s).unwrap();
		unsafe { Vec2::native(ffi::MeasureTextEx(self.ffi(), s.as_ptr(), size, spacing)) }
	}

	pub fn get_chars_supported(&self) -> Vec<char> {
		let mut v = Vec::new();

		unsafe {
			for i in 0..self.ffi().glyphCount {
				let glyph_info = self.ffi().glyphs.offset(i as isize);

				v.push(char::from_u32((*glyph_info).value as u32).unwrap());
			}
		}

		v
	}

	pub fn get_char_info_advance_x(&self, c: char) -> Option<i32> {
		unsafe {
			for i in 0..self.ffi().glyphCount {
				let glyph_info = self.ffi().glyphs.offset(i as isize);

				if char::from_u32((*glyph_info).value as u32).unwrap() == c {
					return Some((*glyph_info).advanceX);
				}
			}
		}

		None
	}

	pub fn set_char_info_advance_x(&self, c: char, x: i32) {
		unsafe {
			for i in 0..self.ffi().glyphCount {
				let glyph_info = self.ffi().glyphs.offset(i as isize);

				if char::from_u32((*glyph_info).value as u32).unwrap() == c {
					(*glyph_info).advanceX = x;
					return;
				}
			}
		}
	}

	pub fn tex<'a>(&self) -> Tex<'a> {
		Tex {
			ffi: self.ffi().texture,
			_phantom: Some(PhantomData),
		}
	}

	pub(crate) fn ffi(&self) -> ffi::Font {
		if let Some(font) = self.0 {
			font
		} else {
			unsafe { ffi::GetFontDefault() }
		}
	}
}

impl Drop for Font {
	fn drop(&mut self) {
		unsafe {
			if let Some(font) = self.0 {
				ffi::UnloadFont(font);
			}
		}
	}
}

pub struct Music(ffi::Music, Option<Vec<u8>>); // data needs to be in scope if it loads from mem

impl Music {
	pub fn load_from_mem(data: Vec<u8>, extension: &str) -> Self {
		let extension = std::ffi::CString::new(extension).unwrap();
		Self(
			unsafe {
				ffi::LoadMusicStreamFromMemory(extension.as_ptr(), data.as_ptr(), data.len() as i32)
			},
			Some(data),
		)
	}

	pub fn play(&mut self) {
		unsafe { ffi::PlayMusicStream(self.0) }
	}

	pub fn stop(&mut self) {
		unsafe { ffi::StopMusicStream(self.0) }
	}

	pub fn update_stream(&mut self) {
		unsafe { ffi::UpdateMusicStream(self.0) }
	}

	pub fn set_volume(&mut self, volume: f32) {
		unsafe { ffi::SetMusicVolume(self.0, volume) }
	}
}

impl Drop for Music {
	fn drop(&mut self) {
		unsafe { ffi::UnloadMusicStream(self.0) }
	}
}

#[derive(Copy, Clone, PartialEq, Debug)]
pub struct Cam2D {
	pub target: Vec2,
	pub offset: Vec2,
	pub rotation: f32,
	pub zoom: f32,
}

impl Cam2D {
	pub fn new() -> Self {
		Self {
			target: Vec2 { x: 0., y: 0. },
			offset: Vec2 { x: 0., y: 0. },
			rotation: 0.,
			zoom: 1.,
		}
	}

	pub fn to_world(self, pos: Vec2) -> Vec2 {
		unsafe { Vec2::native(ffi::GetScreenToWorld2D(pos.ffi(), self.ffi())) }
	}

	pub fn to_screen(self, pos: Vec2) -> Vec2 {
		unsafe { Vec2::native(ffi::GetWorldToScreen2D(pos.ffi(), self.ffi())) }
	}

	pub fn at(self, x: i32, y: i32) -> Self {
		Self {
			target: Vec2 {
				x: x as f32,
				y: y as f32,
				..self.target
			},
			..self
		}
	}

	pub fn rotation(self, rotation: f32) -> Self {
		Self { rotation, ..self }
	}

	pub(crate) fn ffi(self) -> ffi::Camera2D {
		ffi::Camera2D {
			offset: self.offset.ffi(),
			target: self.target.ffi(),
			rotation: self.rotation,
			zoom: self.zoom,
		}
	}
}

#[derive(Copy, Clone, PartialEq, Debug)]
pub struct Circle {
	center: Vec2,
	radius: f32,
}

impl Circle {
	pub fn draw(self) -> builder::DrawCircle {
		builder::DrawCircle::new(self)
	}
}

#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub struct Color {
	pub r: u8,
	pub g: u8,
	pub b: u8,
	pub a: u8,
}

macro_rules! color_const {
	($r:literal, $g:literal, $b:literal, $a:literal, $name:ident) => {
		pub const $name: Self = Self {
			r: $r,
			g: $g,
			b: $b,
			a: $a,
		};
	};
}

impl Color {
	color_const!(200, 200, 200, 255, LIGHT_GRAY);
	color_const!(130, 130, 130, 255, GRAY);
	color_const!(80_, 80_, 80_, 255, DARK_GRAY);
	color_const!(253, 249, 0__, 255, YELLOW);
	color_const!(255, 203, 0__, 255, GOLD);
	color_const!(255, 161, 0__, 255, ORANGE);
	color_const!(255, 109, 194, 255, PINK);
	color_const!(230, 41_, 55_, 255, RED);
	color_const!(190, 33_, 55_, 255, MAROON);
	color_const!(0__, 228, 48_, 255, GREEN);
	color_const!(0__, 158, 47_, 255, LIME);
	color_const!(0__, 117, 44_, 255, DARK_GREEN);
	color_const!(102, 191, 255, 255, SKY_BLUE);
	color_const!(0__, 121, 241, 255, BLUE);
	color_const!(0__, 82_, 172, 255, DARK_BLUE);
	color_const!(200, 122, 255, 255, PURPLE);
	color_const!(135, 60_, 190, 255, VIOLET);
	color_const!(112, 31_, 126, 255, DARK_PURPLE);
	color_const!(211, 176, 131, 255, BEIGE);
	color_const!(127, 106, 79_, 255, BROWN);
	color_const!(76_, 63_, 47_, 255, DARK_BROWN);

	color_const!(255, 255, 255, 255, WHITE);
	color_const!(0__, 0__, 0__, 255, BLACK);
	color_const!(0__, 0__, 0__, 0__, BLANK);
	color_const!(255, 0__, 255, 255, MAGENTA);
	color_const!(245, 245, 245, 255, RAY_WHITE);

	pub(crate) fn ffi(self) -> ffi::Color {
		ffi::Color {
			r: self.r,
			g: self.g,
			b: self.b,
			a: self.a,
		}
	}

	pub(crate) fn native(c: ffi::Color) -> Self {
		Self {
			r: c.r,
			g: c.g,
			b: c.b,
			a: c.a,
		}
	}
}

#[derive(Copy, Clone, PartialEq, Debug)]
pub enum MB {
	Left,
	Middle,
	Right,
	Side,
	Extra,
	Forward,
	Back,
}

impl MB {
	pub fn is_up(self) -> bool {
		unsafe { ffi::IsMouseButtonUp(self.ffi() as i32) }
	}

	pub fn is_down(self) -> bool {
		unsafe { ffi::IsMouseButtonDown(self.ffi() as i32) }
	}

	pub fn is_pressed(self) -> bool {
		unsafe { ffi::IsMouseButtonPressed(self.ffi() as i32) }
	}

	pub(crate) fn ffi(self) -> ffi::MouseButton {
		match self {
			Self::Left => ffi::MouseButton_MOUSE_BUTTON_LEFT,
			Self::Middle => ffi::MouseButton_MOUSE_BUTTON_MIDDLE,
			Self::Right => ffi::MouseButton_MOUSE_BUTTON_RIGHT,
			Self::Side => ffi::MouseButton_MOUSE_BUTTON_SIDE,
			Self::Extra => ffi::MouseButton_MOUSE_BUTTON_EXTRA,
			Self::Forward => ffi::MouseButton_MOUSE_BUTTON_FORWARD,
			Self::Back => ffi::MouseButton_MOUSE_BUTTON_BACK,
		}
	}
}

#[derive(Copy, Clone, PartialEq, Debug)]
#[rustfmt::skip]
pub enum Key {
	LeftBracket, RightBracket,
	Semicolon, Apostrophe, Backslash,
	Comma, Period, Slash,

	Grave, Minus, Equal,

	One, Two, Three, Four, Five,
	Six, Seven, Eight, Nine, Zero,

	A, B, C, D, E, F, G, H, I, J, K, L, M,
	N, O, P, Q, R, S, T, U, V, W, X, Y, Z,

	Space, Escape, Enter,
	Tab, Backspace, Menu,

	Up, Down, Left, Right,

	Insert, Home, PageUp,
	Delete, End, PageDown,

	CapsLock, ScrollLock, NumLock,

	PrintScreen, Pause,

	F1, F2, F3, F4, F5, F6, F7, F8, F9, F10, F11, F12,

	LeftShift, LeftCtrl, LeftAlt, LeftSuper,
	RightShift, RightCtrl, RightAlt, RightSuper,

	KP1, KP2, KP3, KP4, KP5, KP6, KP7, KP8, KP9, KP0,

	KPDivide, KPMultiply, KPSubtract,
	KPAdd, KPEnter, KPDecimal, KPEqual,

	AndroidBack, AndroidMenu,
	AndroidVolumeUp, AndroidVolumeDown,
}

impl Key {
	pub fn is_pressed(self) -> bool {
		unsafe { ffi::IsKeyPressed(self.ffi() as i32) }
	}

	pub fn is_down(self) -> bool {
		unsafe { ffi::IsKeyDown(self.ffi() as i32) }
	}

	pub fn is_up(self) -> bool {
		unsafe { ffi::IsKeyUp(self.ffi() as i32) }
	}

	pub(crate) fn ffi(self) -> ffi::KeyboardKey {
		use Key::*;

		match self {
			A => ffi::KeyboardKey_KEY_A,
			Apostrophe => ffi::KeyboardKey_KEY_APOSTROPHE,
			B => ffi::KeyboardKey_KEY_B,
			AndroidBack => ffi::KeyboardKey_KEY_BACK,
			Backslash => ffi::KeyboardKey_KEY_BACKSLASH,
			Backspace => ffi::KeyboardKey_KEY_BACKSPACE,
			C => ffi::KeyboardKey_KEY_C,
			CapsLock => ffi::KeyboardKey_KEY_CAPS_LOCK,
			Comma => ffi::KeyboardKey_KEY_COMMA,
			D => ffi::KeyboardKey_KEY_D,
			Delete => ffi::KeyboardKey_KEY_DELETE,
			Down => ffi::KeyboardKey_KEY_DOWN,
			E => ffi::KeyboardKey_KEY_E,
			Eight => ffi::KeyboardKey_KEY_EIGHT,
			End => ffi::KeyboardKey_KEY_END,
			Enter => ffi::KeyboardKey_KEY_ENTER,
			Equal => ffi::KeyboardKey_KEY_EQUAL,
			Escape => ffi::KeyboardKey_KEY_ESCAPE,
			F => ffi::KeyboardKey_KEY_F,
			F1 => ffi::KeyboardKey_KEY_F1,
			F2 => ffi::KeyboardKey_KEY_F2,
			F3 => ffi::KeyboardKey_KEY_F3,
			F4 => ffi::KeyboardKey_KEY_F4,
			F5 => ffi::KeyboardKey_KEY_F5,
			F6 => ffi::KeyboardKey_KEY_F6,
			F7 => ffi::KeyboardKey_KEY_F7,
			F8 => ffi::KeyboardKey_KEY_F8,
			F9 => ffi::KeyboardKey_KEY_F9,
			F10 => ffi::KeyboardKey_KEY_F10,
			F11 => ffi::KeyboardKey_KEY_F11,
			F12 => ffi::KeyboardKey_KEY_F12,
			Five => ffi::KeyboardKey_KEY_FIVE,
			Four => ffi::KeyboardKey_KEY_FOUR,
			G => ffi::KeyboardKey_KEY_G,
			Grave => ffi::KeyboardKey_KEY_GRAVE,
			H => ffi::KeyboardKey_KEY_H,
			Home => ffi::KeyboardKey_KEY_HOME,
			I => ffi::KeyboardKey_KEY_I,
			Insert => ffi::KeyboardKey_KEY_INSERT,
			J => ffi::KeyboardKey_KEY_J,
			K => ffi::KeyboardKey_KEY_K,
			Menu => ffi::KeyboardKey_KEY_KB_MENU,
			KP0 => ffi::KeyboardKey_KEY_KP_0,
			KP1 => ffi::KeyboardKey_KEY_KP_1,
			KP2 => ffi::KeyboardKey_KEY_KP_2,
			KP3 => ffi::KeyboardKey_KEY_KP_3,
			KP4 => ffi::KeyboardKey_KEY_KP_4,
			KP5 => ffi::KeyboardKey_KEY_KP_5,
			KP6 => ffi::KeyboardKey_KEY_KP_6,
			KP7 => ffi::KeyboardKey_KEY_KP_7,
			KP8 => ffi::KeyboardKey_KEY_KP_8,
			KP9 => ffi::KeyboardKey_KEY_KP_9,
			KPAdd => ffi::KeyboardKey_KEY_KP_ADD,
			KPDecimal => ffi::KeyboardKey_KEY_KP_DECIMAL,
			KPDivide => ffi::KeyboardKey_KEY_KP_DIVIDE,
			KPEnter => ffi::KeyboardKey_KEY_KP_ENTER,
			KPEqual => ffi::KeyboardKey_KEY_KP_EQUAL,
			KPMultiply => ffi::KeyboardKey_KEY_KP_MULTIPLY,
			KPSubtract => ffi::KeyboardKey_KEY_KP_SUBTRACT,
			L => ffi::KeyboardKey_KEY_L,
			Left => ffi::KeyboardKey_KEY_LEFT,
			LeftAlt => ffi::KeyboardKey_KEY_LEFT_ALT,
			LeftBracket => ffi::KeyboardKey_KEY_LEFT_BRACKET,
			LeftCtrl => ffi::KeyboardKey_KEY_LEFT_CONTROL,
			LeftShift => ffi::KeyboardKey_KEY_LEFT_SHIFT,
			LeftSuper => ffi::KeyboardKey_KEY_LEFT_SUPER,
			M => ffi::KeyboardKey_KEY_M,
			AndroidMenu => ffi::KeyboardKey_KEY_MENU,
			Minus => ffi::KeyboardKey_KEY_MINUS,
			N => ffi::KeyboardKey_KEY_N,
			Nine => ffi::KeyboardKey_KEY_NINE,
			NumLock => ffi::KeyboardKey_KEY_NUM_LOCK,
			O => ffi::KeyboardKey_KEY_O,
			One => ffi::KeyboardKey_KEY_ONE,
			P => ffi::KeyboardKey_KEY_P,
			PageDown => ffi::KeyboardKey_KEY_PAGE_DOWN,
			PageUp => ffi::KeyboardKey_KEY_PAGE_UP,
			Pause => ffi::KeyboardKey_KEY_PAUSE,
			Period => ffi::KeyboardKey_KEY_PERIOD,
			PrintScreen => ffi::KeyboardKey_KEY_PRINT_SCREEN,
			Q => ffi::KeyboardKey_KEY_Q,
			R => ffi::KeyboardKey_KEY_R,
			Right => ffi::KeyboardKey_KEY_RIGHT,
			RightAlt => ffi::KeyboardKey_KEY_RIGHT_ALT,
			RightBracket => ffi::KeyboardKey_KEY_RIGHT_BRACKET,
			RightCtrl => ffi::KeyboardKey_KEY_RIGHT_CONTROL,
			RightShift => ffi::KeyboardKey_KEY_RIGHT_SHIFT,
			RightSuper => ffi::KeyboardKey_KEY_RIGHT_SUPER,
			S => ffi::KeyboardKey_KEY_S,
			ScrollLock => ffi::KeyboardKey_KEY_SCROLL_LOCK,
			Semicolon => ffi::KeyboardKey_KEY_SEMICOLON,
			Seven => ffi::KeyboardKey_KEY_SEVEN,
			Six => ffi::KeyboardKey_KEY_SIX,
			Slash => ffi::KeyboardKey_KEY_SLASH,
			Space => ffi::KeyboardKey_KEY_SPACE,
			T => ffi::KeyboardKey_KEY_T,
			Tab => ffi::KeyboardKey_KEY_TAB,
			Three => ffi::KeyboardKey_KEY_THREE,
			Two => ffi::KeyboardKey_KEY_TWO,
			U => ffi::KeyboardKey_KEY_U,
			Up => ffi::KeyboardKey_KEY_UP,
			V => ffi::KeyboardKey_KEY_V,
			AndroidVolumeDown => ffi::KeyboardKey_KEY_VOLUME_DOWN,
			AndroidVolumeUp => ffi::KeyboardKey_KEY_VOLUME_UP,
			W => ffi::KeyboardKey_KEY_W,
			X => ffi::KeyboardKey_KEY_X,
			Y => ffi::KeyboardKey_KEY_Y,
			Z => ffi::KeyboardKey_KEY_Z,
			Zero => ffi::KeyboardKey_KEY_ZERO,
		}
	}

	pub(crate) fn native(key: ffi::KeyboardKey) -> Option<Self> {
		use Key::*;

		// Weird bug with the compiler
		//#[allow(unreachable_patterns)]
		Some(match key {
			ffi::KeyboardKey_KEY_A => A,
			ffi::KeyboardKey_KEY_APOSTROPHE => Apostrophe,
			ffi::KeyboardKey_KEY_B => B,
			ffi::KeyboardKey_KEY_BACK => AndroidBack,
			ffi::KeyboardKey_KEY_BACKSLASH => Backslash,
			ffi::KeyboardKey_KEY_BACKSPACE => Backspace,
			ffi::KeyboardKey_KEY_C => C,
			ffi::KeyboardKey_KEY_CAPS_LOCK => CapsLock,
			ffi::KeyboardKey_KEY_COMMA => Comma,
			ffi::KeyboardKey_KEY_D => D,
			ffi::KeyboardKey_KEY_DELETE => Delete,
			ffi::KeyboardKey_KEY_DOWN => Down,
			ffi::KeyboardKey_KEY_E => E,
			ffi::KeyboardKey_KEY_EIGHT => Eight,
			ffi::KeyboardKey_KEY_END => End,
			ffi::KeyboardKey_KEY_ENTER => Enter,
			ffi::KeyboardKey_KEY_EQUAL => Equal,
			ffi::KeyboardKey_KEY_ESCAPE => Escape,
			ffi::KeyboardKey_KEY_F => F,
			ffi::KeyboardKey_KEY_F1 => F1,
			ffi::KeyboardKey_KEY_F2 => F2,
			ffi::KeyboardKey_KEY_F3 => F3,
			ffi::KeyboardKey_KEY_F4 => F4,
			ffi::KeyboardKey_KEY_F5 => F5,
			ffi::KeyboardKey_KEY_F6 => F6,
			ffi::KeyboardKey_KEY_F7 => F7,
			ffi::KeyboardKey_KEY_F8 => F8,
			ffi::KeyboardKey_KEY_F9 => F9,
			ffi::KeyboardKey_KEY_F10 => F10,
			ffi::KeyboardKey_KEY_F11 => F11,
			ffi::KeyboardKey_KEY_F12 => F12,
			ffi::KeyboardKey_KEY_FIVE => Five,
			ffi::KeyboardKey_KEY_FOUR => Four,
			ffi::KeyboardKey_KEY_G => G,
			ffi::KeyboardKey_KEY_GRAVE => Grave,
			ffi::KeyboardKey_KEY_H => H,
			ffi::KeyboardKey_KEY_HOME => Home,
			ffi::KeyboardKey_KEY_I => I,
			ffi::KeyboardKey_KEY_INSERT => Insert,
			ffi::KeyboardKey_KEY_J => J,
			ffi::KeyboardKey_KEY_K => K,
			ffi::KeyboardKey_KEY_KB_MENU => Menu,
			ffi::KeyboardKey_KEY_KP_0 => KP0,
			ffi::KeyboardKey_KEY_KP_1 => KP1,
			ffi::KeyboardKey_KEY_KP_2 => KP2,
			ffi::KeyboardKey_KEY_KP_3 => KP3,
			ffi::KeyboardKey_KEY_KP_4 => KP4,
			ffi::KeyboardKey_KEY_KP_5 => KP5,
			ffi::KeyboardKey_KEY_KP_6 => KP6,
			ffi::KeyboardKey_KEY_KP_7 => KP7,
			ffi::KeyboardKey_KEY_KP_8 => KP8,
			ffi::KeyboardKey_KEY_KP_9 => KP9,
			ffi::KeyboardKey_KEY_KP_ADD => KPAdd,
			ffi::KeyboardKey_KEY_KP_DECIMAL => KPDecimal,
			ffi::KeyboardKey_KEY_KP_DIVIDE => KPDivide,
			ffi::KeyboardKey_KEY_KP_ENTER => KPEnter,
			ffi::KeyboardKey_KEY_KP_EQUAL => KPEqual,
			ffi::KeyboardKey_KEY_KP_MULTIPLY => KPMultiply,
			ffi::KeyboardKey_KEY_KP_SUBTRACT => KPSubtract,
			ffi::KeyboardKey_KEY_L => L,
			ffi::KeyboardKey_KEY_LEFT => Left,
			ffi::KeyboardKey_KEY_LEFT_ALT => LeftAlt,
			ffi::KeyboardKey_KEY_LEFT_BRACKET => LeftBracket,
			ffi::KeyboardKey_KEY_LEFT_CONTROL => LeftCtrl,
			ffi::KeyboardKey_KEY_LEFT_SHIFT => LeftShift,
			ffi::KeyboardKey_KEY_LEFT_SUPER => LeftSuper,
			ffi::KeyboardKey_KEY_M => M,
			ffi::KeyboardKey_KEY_MENU => AndroidMenu,
			ffi::KeyboardKey_KEY_MINUS => Minus,
			ffi::KeyboardKey_KEY_N => N,
			ffi::KeyboardKey_KEY_NINE => Nine,
			ffi::KeyboardKey_KEY_NUM_LOCK => NumLock,
			ffi::KeyboardKey_KEY_O => O,
			ffi::KeyboardKey_KEY_ONE => One,
			ffi::KeyboardKey_KEY_P => P,
			ffi::KeyboardKey_KEY_PAGE_DOWN => PageDown,
			ffi::KeyboardKey_KEY_PAGE_UP => PageUp,
			ffi::KeyboardKey_KEY_PAUSE => Pause,
			ffi::KeyboardKey_KEY_PERIOD => Period,
			ffi::KeyboardKey_KEY_PRINT_SCREEN => PrintScreen,
			ffi::KeyboardKey_KEY_Q => Q,
			ffi::KeyboardKey_KEY_R => R,
			ffi::KeyboardKey_KEY_RIGHT => Right,
			ffi::KeyboardKey_KEY_RIGHT_ALT => RightAlt,
			ffi::KeyboardKey_KEY_RIGHT_BRACKET => RightBracket,
			ffi::KeyboardKey_KEY_RIGHT_CONTROL => RightCtrl,
			ffi::KeyboardKey_KEY_RIGHT_SHIFT => RightShift,
			ffi::KeyboardKey_KEY_RIGHT_SUPER => RightSuper,
			ffi::KeyboardKey_KEY_S => S,
			ffi::KeyboardKey_KEY_SCROLL_LOCK => ScrollLock,
			ffi::KeyboardKey_KEY_SEMICOLON => Semicolon,
			ffi::KeyboardKey_KEY_SEVEN => Seven,
			ffi::KeyboardKey_KEY_SIX => Six,
			ffi::KeyboardKey_KEY_SLASH => Slash,
			ffi::KeyboardKey_KEY_SPACE => Space,
			ffi::KeyboardKey_KEY_T => T,
			ffi::KeyboardKey_KEY_TAB => Tab,
			ffi::KeyboardKey_KEY_THREE => Three,
			ffi::KeyboardKey_KEY_TWO => Two,
			ffi::KeyboardKey_KEY_U => U,
			ffi::KeyboardKey_KEY_UP => Up,
			ffi::KeyboardKey_KEY_V => V,
			ffi::KeyboardKey_KEY_VOLUME_DOWN => AndroidVolumeDown,
			ffi::KeyboardKey_KEY_VOLUME_UP => AndroidVolumeUp,
			ffi::KeyboardKey_KEY_W => W,
			ffi::KeyboardKey_KEY_X => X,
			ffi::KeyboardKey_KEY_Y => Y,
			ffi::KeyboardKey_KEY_Z => Z,
			ffi::KeyboardKey_KEY_ZERO => Zero,
			_ => return None,
		})
	}
}

#[derive(Copy, Clone, PartialEq, Debug)]
pub enum TexFilter {
	Point,
	Bilinear,
	Trilinear,
	Anisotropic4x,
	Anisotropic8x,
	Anisotropic16x,
}

impl TexFilter {
	pub(crate) fn ffi(self) -> i32 {
		(match self {
			Self::Point => ffi::TextureFilter_TEXTURE_FILTER_POINT,
			Self::Bilinear => ffi::TextureFilter_TEXTURE_FILTER_BILINEAR,
			Self::Trilinear => ffi::TextureFilter_TEXTURE_FILTER_TRILINEAR,
			Self::Anisotropic4x => ffi::TextureFilter_TEXTURE_FILTER_ANISOTROPIC_4X,
			Self::Anisotropic8x => ffi::TextureFilter_TEXTURE_FILTER_ANISOTROPIC_8X,
			Self::Anisotropic16x => ffi::TextureFilter_TEXTURE_FILTER_ANISOTROPIC_16X,
		}) as i32
	}

	pub(crate) fn native(filter: i32) -> TexFilter {
		match filter as u32 {
			ffi::TextureFilter_TEXTURE_FILTER_POINT => Self::Point,
			ffi::TextureFilter_TEXTURE_FILTER_BILINEAR => Self::Bilinear,
			ffi::TextureFilter_TEXTURE_FILTER_TRILINEAR => Self::Trilinear,
			ffi::TextureFilter_TEXTURE_FILTER_ANISOTROPIC_4X => Self::Anisotropic4x,
			ffi::TextureFilter_TEXTURE_FILTER_ANISOTROPIC_8X => Self::Anisotropic8x,
			ffi::TextureFilter_TEXTURE_FILTER_ANISOTROPIC_16X => Self::Anisotropic16x,
			_ => unreachable!(),
		}
	}
}

#[derive(Copy, Clone, PartialEq, Debug)]
pub enum LogLevel {
	All,
	Trace,
	Debug,
	Info,
	Warning,
	Error,
	Fatal,
	None,
}

impl LogLevel {
	pub fn set_minimum(&self) {
		let ll = match self {
			Self::All => ffi::TraceLogLevel_LOG_ALL,
			Self::Trace => ffi::TraceLogLevel_LOG_TRACE,
			Self::Debug => ffi::TraceLogLevel_LOG_DEBUG,
			Self::Info => ffi::TraceLogLevel_LOG_INFO,
			Self::Warning => ffi::TraceLogLevel_LOG_WARNING,
			Self::Error => ffi::TraceLogLevel_LOG_ERROR,
			Self::Fatal => ffi::TraceLogLevel_LOG_FATAL,
			Self::None => ffi::TraceLogLevel_LOG_NONE,
		};

		unsafe {
			ffi::SetTraceLogLevel(ll as libc::c_int);
		}
	}
}

#[derive(Copy, Clone)]
pub struct NPatch {
	pub src: Rec,
	pub left: i32,
	pub top: i32,
	pub right: i32,
	pub bottom: i32,
	pub layout: NPatchType,
}

#[derive(Copy, Clone, PartialEq)]
pub enum NPatchType {
	Nine,
	Vertical,
	Horizontal,
}

#[derive(Copy, Clone, PartialEq, Debug)]
pub enum Gesture {
	Tap,
	// ...
}

impl Gesture {
	pub fn detected(self) -> bool {
		unsafe { ffi::IsGestureDetected(self.ffi() as i32) }
	}

	pub(crate) fn ffi(self) -> ffi::Gesture {
		match self {
			Gesture::Tap => ffi::Gesture_GESTURE_TAP,
		}
	}
}

#[macro_export]
macro_rules! use_shader {
	($i:expr => $e:expr) => {
		$i.begin();
		$e;
		rl::Shader::end();
	};
}

#[macro_export]
macro_rules! with_camera_2d {
	($cam:expr => $e:expr) => {{
		unsafe { $crate::begin_camera_2d_mode($cam) };
		let _var = {
			struct S();
			impl ::std::ops::Drop for S {
				fn drop(&mut self) {
					unsafe { $crate::end_camera_2d_mode() }
				}
			}
			S()
		};
		$e
	}};
}

#[macro_export]
macro_rules! draw_onto {
	($rt:expr => $e:expr) => {{
		unsafe { $crate::begin_texture_mode($rt) };
		let _var = {
			struct S();
			impl ::std::ops::Drop for S {
				fn drop(&mut self) {
					unsafe { $crate::end_texture_mode() }
				}
			}
			S()
		};
		$e
	}};

	($rt:expr =>) => {{
		unsafe { $crate::begin_texture_mode(*$rt) };
		let _var = {
			struct S();
			impl ::std::ops::Drop for S {
				fn drop(&mut self) {
					unsafe { $crate::end_texture_mode() }
				}
			}
			S()
		};
	}};
}

#[macro_export]
macro_rules! draw {
	(=>) => {
		$crate::begin_drawing();
		let _var = {
			struct S();
			impl ::std::ops::Drop for S {
				fn drop(&mut self) {
					unsafe { $crate::end_drawing() }
				}
			}
			S()
		};
	};

	($e:expr) => {{
		$crate::begin_drawing();
		let _var = {
			struct S();
			impl ::std::ops::Drop for S {
				fn drop(&mut self) {
					unsafe { $crate::end_drawing() }
				}
			}
			S()
		};
		$e
	}};
}

#[macro_export]
macro_rules! start_graphics {
	($width:expr, $height:expr, $title:expr) => {
		unsafe { $crate::init_window($width, $height, $title) };
		let _var = {
			struct S();
			impl ::std::ops::Drop for S {
				fn drop(&mut self) {
					unsafe { $crate::deinit_window() }
				}
			}
		};
	};

	($width:expr, $height:expr, $title:expr,) => {
		unsafe { $crate::init_window($width, $height, $title) };
		let _var = {
			struct S();
			impl ::std::ops::Drop for S {
				fn drop(&mut self) {
					unsafe { $crate::deinit_window() }
				}
			}
		};
	};

	($width:expr, $height:expr, $title:expr => $e:expr) => {{
		unsafe { $crate::init_window($width, $height, $title) };
		let _var = {
			struct S();
			impl ::std::ops::Drop for S {
				fn drop(&mut self) {
					unsafe { $crate::deinit_window() }
				}
			}
		};
		$e;
	}};
}

#[macro_export]
macro_rules! start_audio {
	(=>) => {
		unsafe { $crate::init_audio_device() };
		let _var = {
			struct S();
			impl ::std::ops::Drop for S {
				fn drop(&mut self) {
					unsafe { $crate::deinit_audio_device() }
				}
			}
		};
	};

	($e:expr) => {{
		{
			unsafe { $crate::init_audio_device() };
			let _var = {
				struct S();
				impl ::std::ops::Drop for S {
					fn drop(&mut self) {
						unsafe { $crate::deinit_audio_device() }
					}
				}
			};
			$e
		}
	}};
}

macro_rules! setter_and_getter_flags {
	($const:ident, $set:ident, $get:ident) => {
		pub fn $set(b: bool) {
			unsafe {
				if b {
					ffi::SetWindowState(ffi::$const);
				} else {
					ffi::ClearWindowState(ffi::$const);
				}
			}
		}

		pub fn $get() -> bool {
			unsafe { ffi::IsWindowState(ffi::$const) }
		}
	};
}

#[doc(hidden)]
pub unsafe fn init_window(width: i32, height: i32, title: &str) {
	let s = title.to_owned() + "\0";

	unsafe {
		ffi::InitWindow(width, height, s.as_ptr() as *const i8);
	}
}

#[doc(hidden)]
pub unsafe fn deinit_window() {
	unsafe {
		ffi::CloseWindow();
	}
}

#[doc(hidden)]
pub unsafe fn init_audio_device() {
	unsafe {
		ffi::InitAudioDevice();
	}
}

#[doc(hidden)]
pub unsafe fn deinit_audio_device() {
	unsafe {
		ffi::CloseAudioDevice();
	}
}

#[doc(hidden)]
pub unsafe fn begin_texture_mode(rt: &RenderTex) {
	ffi::BeginTextureMode(rt.0)
}

#[doc(hidden)]
pub unsafe fn end_texture_mode() {
	ffi::EndTextureMode()
}

#[doc(hidden)]
pub unsafe fn begin_camera_2d_mode(cam: Cam2D) {
	ffi::BeginMode2D(cam.ffi());
}

#[doc(hidden)]
pub unsafe fn end_camera_2d_mode() {
	ffi::EndMode2D();
}

pub fn set_target_fps(i: i32) {
	unsafe {
		ffi::SetTargetFPS(i);
	}
}

setter_and_getter_flags!(ConfigFlags_FLAG_VSYNC_HINT, set_vsync_hint, get_vsync_hint);
setter_and_getter_flags!(
	ConfigFlags_FLAG_MSAA_4X_HINT,
	set_msaa_4x_hint,
	get_msaa_4x_hint
);
setter_and_getter_flags!(
	ConfigFlags_FLAG_WINDOW_HIDDEN,
	set_window_hidden,
	get_window_hidden
);
setter_and_getter_flags!(
	ConfigFlags_FLAG_WINDOW_HIGHDPI,
	set_window_highdpi,
	get_window_highdpi
);
setter_and_getter_flags!(
	ConfigFlags_FLAG_WINDOW_TOPMOST,
	set_window_topmost,
	get_window_topmost
);
setter_and_getter_flags!(
	ConfigFlags_FLAG_FULLSCREEN_MODE,
	set_fullscreen_mode,
	get_fullscreen_mode
);
setter_and_getter_flags!(
	ConfigFlags_FLAG_INTERLACED_HINT,
	set_interlaced_hint,
	get_interlaced_hint
);
setter_and_getter_flags!(
	ConfigFlags_FLAG_WINDOW_MAXIMIZED,
	set_window_maximized,
	get_window_maximized
);
setter_and_getter_flags!(
	ConfigFlags_FLAG_WINDOW_MINIMIZED,
	set_window_minimized,
	get_window_minimized
);
setter_and_getter_flags!(
	ConfigFlags_FLAG_WINDOW_RESIZABLE,
	set_window_resizable,
	get_window_resizable
);
setter_and_getter_flags!(
	ConfigFlags_FLAG_WINDOW_UNFOCUSED,
	set_window_unfocused,
	get_window_unfocused
);
setter_and_getter_flags!(
	ConfigFlags_FLAG_WINDOW_ALWAYS_RUN,
	set_window_always_run,
	get_window_always_run
);
setter_and_getter_flags!(
	ConfigFlags_FLAG_WINDOW_TRANSPARENT,
	set_window_transparent,
	get_window_transparent
);
setter_and_getter_flags!(
	ConfigFlags_FLAG_WINDOW_UNDECORATED,
	set_window_undecorated,
	get_window_undecorated
);
setter_and_getter_flags!(
	ConfigFlags_FLAG_WINDOW_MOUSE_PASSTHROUGH,
	set_window_mouse_passthrough,
	get_window_mouse_passthrough
);

pub fn set_cursor_hidden(b: bool) {
	unsafe {
		if b {
			ffi::HideCursor();
		} else {
			ffi::ShowCursor();
		}
	}
}

pub fn get_cursor_hidden() -> bool {
	unsafe { ffi::IsCursorHidden() }
}

pub fn mouse_scale_set(x: f32, y: f32) {
	unsafe { ffi::SetMouseScale(x, y) }
}

pub fn mouse_offset_set(x: i32, y: i32) {
	unsafe { ffi::SetMouseOffset(x, y) }
}

pub fn mouse_delta() -> Vec2 {
	unsafe { Vec2::native(ffi::GetMouseDelta()) }
}

pub fn window_should_close() -> bool {
	unsafe { ffi::WindowShouldClose() }
}

pub fn toggle_fullscreen() {
	unsafe { ffi::ToggleFullscreen() }
}

pub fn begin_drawing() {
	unsafe { ffi::BeginDrawing() }
}

pub fn end_drawing() {
	unsafe { ffi::EndDrawing() }
}

pub fn clear_background(c: Color) {
	unsafe { ffi::ClearBackground(c.ffi()) }
}

pub fn get_screen_height() -> i32 {
	unsafe { ffi::GetScreenHeight() }
}

pub fn get_screen_width() -> i32 {
	unsafe { ffi::GetScreenWidth() }
}

pub fn get_time() -> f64 {
	unsafe { ffi::GetTime() }
}

pub fn get_frame_time() -> f64 {
	unsafe { ffi::GetFrameTime() as f64 }
}

pub fn get_fps() -> i32 {
	unsafe { ffi::GetFPS() }
}

pub fn get_mouse_position() -> Vec2 {
	unsafe { Vec2::native(ffi::GetMousePosition()) }
}

pub fn get_mouse_wheel_move() -> f32 {
	unsafe { ffi::GetMouseWheelMove() }
}

pub fn unbind_exit_key() {
	unsafe { ffi::SetExitKey(0) }
}

pub fn set_window_minimum_size(width: i32, height: i32) {
	unsafe { ffi::SetWindowMinSize(width, height) }
}

pub fn get_random_value(min: i32, max: i32) -> i32 {
	unsafe { ffi::GetRandomValue(min, max) }
}

pub fn is_file_dropped() -> bool {
	unsafe { ffi::IsFileDropped() }
}

pub fn get_files_dropped() -> Vec<String> {
	use std::ffi::CStr;

	let mut files = Vec::new();

	unsafe {
		let rl_files = ffi::LoadDroppedFiles();

		for i in 0..rl_files.count {
			let s = rl_files.paths.offset(i as isize) as *const *const i8;

			let s = if let Ok(s) = CStr::from_ptr(*s).to_str() {
				s
			} else {
				continue;
			};

			files.push(s.to_owned());
		}

		ffi::UnloadDroppedFiles(rl_files);
	}

	files
}
